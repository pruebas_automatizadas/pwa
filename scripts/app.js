(function () {
    'use strict';

    var app = {
        isLoading: true,
        visibleCards: {},
        selectedTimetables: [],
        spinner: document.querySelector('.loader'),
        cardTemplate: document.querySelector('.cardTemplate'),
        container: document.querySelector('.main'),
        addDialog: document.querySelector('.dialog-container'),
        preferencesTable: "preferences"
    };

    // This works on all devices/browsers, and uses IndexedDBShim as a final fallback 
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;

    // Open (or create) the database
    var open = indexedDB.open("MyDatabase", 1);
    // Create the schema
    open.onupgradeneeded = function () {
        var db = open.result;
        var store = db.createObjectStore(app.preferencesTable, { keyPath: "url" });
    };
    /*****************************************************************************
     *
     * Event listeners for UI elements
     *
     ****************************************************************************/

    document.getElementById('butRefresh').addEventListener('click', function () {
        // Refresh all of the metro stations
        app.updateSchedules();
    });

    document.getElementById('butAdd').addEventListener('click', function () {
        // Open/show the add new station dialog
        app.toggleAddDialog(true);
    });

    document.getElementById('butAddCity').addEventListener('click', function () {
        var select = document.getElementById('selectTimetableToAdd');
        var selected = select.options[select.selectedIndex];
        var key = selected.value;
        var label = selected.textContent;
        if (!app.selectedTimetables) {
            app.selectedTimetables = [];
        }
        app.getSchedule(key, label);
        app.selectedTimetables.push({ key: key, label: label });
        app.saveSelectedTimeTables();
        app.toggleAddDialog(false);
    });

    document.getElementById('butAddCancel').addEventListener('click', function () {
        // Close the add new station dialog
        app.toggleAddDialog(false);
    });


    /*****************************************************************************
     *
     * Methods to update/refresh the UI
     *
     ****************************************************************************/

    // Toggles the visibility of the add new station dialog.
    app.toggleAddDialog = function (visible) {
        if (visible) {
            app.addDialog.classList.add('dialog-container--visible');
        } else {
            app.addDialog.classList.remove('dialog-container--visible');
        }
    };

    // Updates a timestation card with the latest weather forecast. If the card
    // doesn't already exist, it's cloned from the template.

    app.updateTimetableCard = function (data) {
        var key = data.key;
        var dataLastUpdated = new Date(data.created);
        var schedules = data.schedules;
        var card = app.visibleCards[key];

        if (!card) {
            var label = data.label.split(', ');
            var title = label[0];
            var subtitle = label[1];
            card = app.cardTemplate.cloneNode(true);
            card.classList.remove('cardTemplate');
            card.querySelector('.label').textContent = title;
            card.querySelector('.subtitle').textContent = subtitle;
            card.removeAttribute('hidden');
            app.container.appendChild(card);
            app.visibleCards[key] = card;
        }
        card.querySelector('.card-last-updated').textContent = data.created;

        var scheduleUIs = card.querySelectorAll('.schedule');
        for (var i = 0; i < 4; i++) {
            var schedule = schedules[i];
            var scheduleUI = scheduleUIs[i];
            if (schedule && scheduleUI) {
                scheduleUI.querySelector('.message').textContent = schedule.message;
            }
        }

        if (app.isLoading) {
            window.cardLoadTime = performance.now();
            app.spinner.setAttribute('hidden', true);
            app.container.removeAttribute('hidden');
            app.isLoading = false;
        }
    };

    /*****************************************************************************
     *
     * Methods for dealing with the model
     *
     ****************************************************************************/


    app.getSchedule = function (key, label) {
        var url = 'https://api-ratp.pierre-grimaud.fr/v3/schedules/' + key;

        if ('caches' in window) {
            console.log("Getting from cache");
            console.log("Getting from cache222");
            caches.match(url).then(function (response) {
                if (response) {
                    response.json().then(function updateFromCache(json) {
                        var response = json;
                        var result = {};
                        result.key = key;
                        result.label = label;
                        result.created = response._metadata.date;
                        result.schedules = response.result.schedules;
                        app.updateTimetableCard(result);
                    });
                }
            });
        }

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status === 200) {
                    var response = JSON.parse(request.response);
                    var result = {};
                    result.key = key;
                    result.label = label;
                    result.created = response._metadata.date;
                    result.schedules = response.result.schedules;
                    app.updateTimetableCard(result);
                }
            } else {
                // Return the initial weather forecast since no data is available.
                app.updateTimetableCard(initialStationTimetable);
            }
        };
        request.open('GET', url);
        request.send();
    };

    // Iterate all of the cards and attempt to get the latest timetable data
    app.updateSchedules = function () {
        var keys = Object.keys(app.visibleCards);
        keys.forEach(function (key) {
            app.getSchedule(key);
        });
    };

    app.getPreferences = function (callback) {
        var open = indexedDB.open("MyDatabase", 1);
        var preferencesTable = "preferences";
        open.onsuccess = function () {
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction(preferencesTable, "readwrite");
            var store = tx.objectStore(preferencesTable);

            var preferences = store.get("1");

            preferences.onsuccess = function () {
                let result = preferences.result ? preferences.result.value : "";
                console.log("preferencias", result);
                callback(result);
            };

        }
    }

    app.saveSelectedTimeTables = function () {
        var selectedTimetables = JSON.stringify(app.selectedTimetables);
        app.updatePreferences(selectedTimetables);
    };

    /*
     * Fake timetable data that is presented when the user first uses the app,
     * or when the user has not saved any stations. See startup code for more
     * discussion.
     */

    var initialStationTimetable = {

        key: 'metros/1/bastille/A',
        label: 'Bastille, Direction La Défense',
        created: '2017-07-18T17:08:42+02:00',
        schedules: [
            {
                message: '0 mn'
            },
            {
                message: '2 mn'
            },
            {
                message: '5 mn'
            }
        ]


    };

    app.getPreferences(function (preferences) {
        app.selectedTimetables = preferences;

        if (app.selectedTimetables) {
            app.selectedTimetables = JSON.parse(app.selectedTimetables);
            app.selectedTimetables.forEach(function (timetable) {
                app.getSchedule(timetable.key, timetable.label);
            });
        } else {
            /* The user is using the app for the first time, or the user has not
             * saved any cities, so show thasdasdasdasdase user adasdsome fake data. A real app in this
             * scenario could guess the user's location via IP asdlookup and then inject
             * that data into the page.
             */
            app.getSchedule('metros/1/bastille/A', 'Bastille, Direction La Défense');
            app.selectedTimetables = [
                { key: initialStationTimetable.key, label: initialStationTimetable.label }
            ];
            app.saveSelectedTimeTables();
        }
    });


    if ('serviceWorker' in navigator) {
        navigator.serviceWorker
            .register('./service-worker.js')
            .then(function () { console.log('Service Worker Registered'); });
        navigator.serviceWorker
            .register('./sw.js')
            .then(function () { console.log('Service Worker Registered'); });
    }


    /*
        open.onsuccess = function () {
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction(app.preferencesTable, "readwrite");
            var store = tx.objectStore(app.preferencesTable);
    
            store.add({ url: "1", value: "" });
            // Add some data
             store.add({ url: "12345", name: { first: "John", last: "Doe" }, age: 42 });
             store.add({ url: "67890", name: { first: "Bob", last: "Smith" }, age: 35 });
             store.add({ url: "111", name: { first: "Boba", last: "Smitah" }, age: 3213 });
             store.add({ url: "222", name: { first: "Boba", last: "Smitah" }, age: 352 });
     
            
    
            // Query the data
            var getJohn = store.get("12345");
    
            getJohn.onsuccess = function () {
                console.log(getJohn.result.name.first);  // => "John"
                getJohn.result.age = 2222;
                var putExample = store.put(getJohn.result);
                putExample.onsuccess = function (event) {
                    console.log("putExample 67890");  // => "John"
                };
            };
    
            var deleteExample = store.delete("67890");
            deleteExample.onsuccess = function (event) {
                console.log("Deleted 67890");  // => "John"
            };
    
            // Close the db when the transaction is done
            tx.oncomplete = function () {
                db.close();
            };
        }
    
             */
    app.updatePreferences = function (preferences) {
        console.log("Updating preferences", app.selectedTimetables);
        var open = indexedDB.open("MyDatabase", 1);
        var preferencesTable = "preferences";
        open.onsuccess = function () {
            // Start a new transaction
            var db = open.result;
            var tx = db.transaction(preferencesTable, "readwrite");
            var store = tx.objectStore(preferencesTable);

            // Add some data
            store.put({ url: "1", value: preferences });

        }

    }


})();
